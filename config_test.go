package jira

import (
	"testing"

	"github.com/stretchr/testify/require"
)

func TestConfig_GetBasicAuth(t *testing.T) {
	r := require.New(t)

	c := Config{
		Email: "dummy@dummy.com",
		Token: "abcdef123456",
	}

	// base64 encoded value of the "<email>:<token>"
	expected := "Basic ZHVtbXlAZHVtbXkuY29tOmFiY2RlZjEyMzQ1Ng=="

	r.Equal(expected, c.GetBasicAuth(), "Should have the expected basic auth value")
}
