package jira

import (
	"encoding/json"
	"fmt"
	"io"
	"net/http"
)

type Jira struct {
	config Config
}

type KeyValue struct {
	Id    int    `json:"id"`
	Value string `json:"value"`
}

type IssueFields struct {
	Summary     string   `json:"summary"`
	CustomField KeyValue `json:"customfield_10040"`
}

type Issue struct {
	ID     string      `json:"id"`
	Key    string      `json:"key"`
	Fields IssueFields `json:"fields"`
}

func New(c Config) *Jira {
	return &Jira{
		config: c,
	}
}

func (j *Jira) GetIssue(k string) (issue *Issue, err error) {
	url := fmt.Sprintf("%s/issue/%s?fields=summary,customfield_10040", j.config.Endpoint, k)

	req, err := http.NewRequest("GET", url, nil)
	if err != nil {
		return nil, fmt.Errorf("error initialising request: %w", err)
	}
	req.Header.Set("Authorization", j.config.GetBasicAuth())

	client := &http.Client{}

	resp, err := client.Do(req)
	if err != nil {
		return nil, fmt.Errorf("error fetching URL: %w", err)
	}
	defer func() {
		if err := resp.Body.Close(); err != nil {
			fmt.Printf("error closing body: %v", err)
		}
	}()

	if resp.StatusCode != http.StatusOK {
		return nil, fmt.Errorf("unexpected status code: %d", resp.StatusCode)
	}

	bs, err := io.ReadAll(resp.Body)
	if err != nil {
		return nil, fmt.Errorf("error reading response: %w", err)
	}

	err = json.Unmarshal(bs, &issue)
	if err != nil {
		return nil, fmt.Errorf("error parsing response: %w", err)
	}

	return
}
