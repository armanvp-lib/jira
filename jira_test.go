package jira

import (
	"os"
	"testing"

	"github.com/stretchr/testify/require"
)

func TestJira_GetIssue(t *testing.T) {
	r := require.New(t)

	c := Config{
		Endpoint: "https://msts-eng.atlassian.net/rest/api/3",
		Email:    os.Getenv("EMAIL"),
		Token:    os.Getenv("TOKEN"),
	}

	jira := New(c)
	i, err := jira.GetIssue("INT-1")
	r.NoError(err, "GetIssue should return no error")
	r.Equal("11141", i.ID, "Should have the expected ID")
	r.Equal("INT-1", i.Key, "Should have the expected Key")
	r.Equal("Meetings/Ceremonies/Events ", i.Fields.Summary, "Should have the expected Summary")
}
