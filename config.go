package jira

import (
	"encoding/base64"
	"fmt"
)

type Config struct {
	Endpoint string
	Email    string
	Token    string
}

func (c Config) GetBasicAuth() string {
	cred := fmt.Sprintf("%s:%s", c.Email, c.Token)
	auth := fmt.Sprintf("Basic %s", base64.StdEncoding.EncodeToString([]byte(cred)))

	return auth
}
